class Customer < ActiveRecord::Base
  validates_formatting_of :phone_num, using: :us_phone
  validates_formatting_of :email, using: :email
end
